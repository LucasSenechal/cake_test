<?php 

require_once('header.php');

session_start();

//require our database connection file
require('conn.php');


$phone = strtolower($conn->real_escape_string($_POST['phone_number']));
$email = $_SESSION['email'];

//query database to check if email has already been registered
$query = "SELECT * FROM `Contest_Entry` WHERE email='$email'";

//declare boolean which will determine if user was successfully registered
$success = false;
$message = null;

//check if query executes
if ($result = $conn->query($query)) {

    //check if query returned any results
    //if the query returned no results, then the email is available to register
    if ($result->num_rows > 0) {

        $date_created = date("Y/m/d");

        //setup db insert query
        $insertQuery = "UPDATE `Contest_Entry` SET `phone`='$phone' WHERE `email` = '$email' ";

        // //execute query
        if ($insertResult = $conn->query($insertQuery)) {
            
            $_SESSION['email'] = $email;
            
            $success = true;
            $message = "";

        } else {
            $message = "<br/> You haven't entered round 1 yet.";
        }

    } else {
        $message = "This email is already associated with an account";
    }

} else {
    $message = "<br/> Could not execute query.";
}

// echo json_encode(array('user' => array('first_name' => $_SESSION['first_name']), 'success' => $success, 'message' => $message));

$conn->close();



?>

<div>


    <form class='form-box' action="thankyou.php" method="POST" id="payment-form">

        <div class="form-row">

            <div>
                <h2 class='thanks-text' >You've been Entered x2!</h2>
            </div>  

            <div>
                <h1 class='header-text'> Triple Your Chances of Winning!</h1>
            </div>  

            <div>
                <h2 class='tagline-text' >Enter your birthday to receive another entry!</h2>
            </div>  

            <div class='form-box-size'>
                <input class='form-control' type='date' name='birthday' required />
            </div>

            <div>
                <h2 class='note-text' > We like to send our followers special things on their birthday.</h2>
            </div> 

            <div class='spacer-50'></div>

          <input type='submit' value='Add a Third Entry' class='submit' />

    

    </form>

</div>