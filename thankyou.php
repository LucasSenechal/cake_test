<?php 

require_once('header.php');

session_start();

//require our database connection file
require('conn.php');


$birthday = strtolower($conn->real_escape_string($_POST['birthday']));
$email = $_SESSION['email'];

//query database to check if email has already been registered
$query = "SELECT * FROM `Contest_Entry` WHERE email='$email'";

//declare boolean which will determine if user was successfully registered
$success = false;
$message = null;

//check if query executes
if ($result = $conn->query($query)) {

    //check if query returned any results
    //if the query returned no results, then the email is available to register
    if ($result->num_rows > 0) {

        $date_created = date("Y/m/d");

        //setup db insert query
        $insertQuery = "UPDATE `Contest_Entry` SET `birthday`='$birthday' WHERE `email` = '$email' ";

        // //execute query
        if ($insertResult = $conn->query($insertQuery)) {
            
            $_SESSION['email'] = $email;
            
            $success = true;
            $message = "";

        } else {
            $message = "<br/> You haven't entered round 1 yet.";
        }

    } else {
        $message = "This email is already associated with an account";
    }

} else {
    $message = "<br/> Could not execute query.";
}

// echo json_encode(array('user' => array('first_name' => $_SESSION['first_name']), 'success' => $success, 'message' => $message));

$conn->close();

?>
<div>

    <div>
        <h2 class='thanks-text' >Congraduations!</h2>
    </div>

    <div>
        <h1 class='thanks'>You've Been Entered 3 Times Into Our Contest!</h1>
    </div> 

    <div>
        <h2 class='tagline-text' >We'll reach out to you once we've selected a winner.</h2>
    </div>

    <a href="http://www.shopssh.com" target="_blank">
        <button class='button' type="button">Complete</button>
    </a>

</div>