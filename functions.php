<?php

function getEntries($clientId) {

    require('conn.php');
    
    $query = "SELECT * FROM `users` WHERE user_id='$id'";

    $details = [];
    $success = false;

    if ($result = $conn->query($query)) {

        if ($row = $result->fetch_assoc()) {

            foreach($row as $key => $value) {
                $details[$key] = trim($value);
            }

            if ($id == $_SESSION['user_id']) {
                $_SESSION['first_name'] = $details['first_name'];
                $_SESSION['last_name'] = $details['last_name'];
                $_SESSION['email'] = $details['email'];
            }

            $success = true;

        }

    }

    $conn->close();

    return json_encode(array(
        'details' => $details
        ,'success' => $success
    ));

}



















function getUserDetails($id = '') {

        require('conn.php');
        
        $id = (empty($id)) ? $_SESSION['user_id'] : $id;
        $query = "SELECT * FROM `users` WHERE user_id='$id'";
    
        $details = [];
        $success = false;
    
        if ($result = $conn->query($query)) {
    
            if ($row = $result->fetch_assoc()) {

                foreach($row as $key => $value) {
                    $details[$key] = trim($value);
                }

                if ($id == $_SESSION['user_id']) {
                    $_SESSION['first_name'] = $details['first_name'];
                    $_SESSION['last_name'] = $details['last_name'];
                    $_SESSION['email'] = $details['email'];
                }
    
                $success = true;
    
            }
    
        }

        $conn->close();
    
        return json_encode(array(
            'details' => $details
            ,'success' => $success
        ));

    }

    function updateProfile($fields) {

        require('conn.php');
        
        $user_id = $_SESSION['user_id'];

        extract($fields);

        $query = "UPDATE users SET first_name='$first_name', last_name='$last_name', email='$email', phone_number='$phone_number', company='$company' WHERE user_id='$user_id'";

        $first = true;
        $success = false;
        $message = null;

        if ($result = $conn->query($query)) {

            $success = true;

        } else {
            $message = "Could not update";
        }

        $conn->close();

        return json_encode(array('success' => $success, 'message' => $message));

    }

   

    




    

   

  

    function logout() {

        //unsets $_SESSION variable so nothing of the user is retained
        $_SESSION['user_id'] = null;
        $_SESSION['email'] = null;
        $_SESSION['first_name'] = null;
        $_SESSION['last_name'] = null;
        session_unset();
        session_destroy();
        header("Location: login.php");

    }



  

  


    /**EMAILS**/

    /*email functions*/

    function sendEmail($email_address, $subject, $msg) {

        $headers  = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
    
        // Additional headers
        //$headers .= 'To: '.$row['first_name'].' '.$row['last_name'].' <'.$row['email'].'>' . '\r\n';
        $headers .= "From: support@myclientcaller.com" . "\r\n";
        $headers .= "Reply-To: support@myclientcaller.com" . "\r\n";
        $headers .= "X-Mailer: PHP/" . phpversion();
    
        mail($email_address, $subject, $msg, $headers);

    }

    function sendWelcomeEmail($email) {

        $subject = 'Welcome to Client Caller!';
    
        $msg = '
            <table align="center" width="100%" cellpadding="0" cellspacing="0" border="0" data-mobile="true" dir="ltr" data-width="600" style="font-size: 16px; background-color: rgb(251, 251, 251);">
            <tbody><tr>
            <td align="center" valign="top" style="margin:0;padding:32px 0 62px;">
            <table align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" width="600" class="wrapper" style="width: 600px;">
            <tbody>
            <tr><td align="center" valign="top" style="margin: 0px; padding: 0px;"><table cellpadding="0" cellspacing="0" align="center" data-editable="image" data-mobile-width="1" width="100%" style="max-width: 100% !important;"><tbody><tr><td valign="top" align="center" style="display: inline-block; padding: 10px 0px; margin: 0px;" class="tdBlock"><img src="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611691203.png?img1509488746619" width="600" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block; width: 100%; max-width: 100% !important;" data-src="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611691203.png|600|110|286|68|0|0|1" data-origsrc="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611688503.png?_ga=2.38551523.1517448460.1509487344-68529623.1509487344"></td></tr></tbody></table></td></tr><tr>
            <td align="left" valign="top" style="margin:0;padding:0;">
            <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" data-editable="text" class="text-block">
            <tbody><tr>
            <td valign="top" align="left" class="lh-2" style="padding: 0px 50px; margin: 0px; line-height: 1.25; font-size: 16px; font-family: Times New Roman, Times, serif;">
            <span style="font-family: Arial,sans-serif; font-size:28px;font-weight:700; color:#000000; line-height:1.2;">
            Welcome to the Client Caller dashboard!</span></td>
            </tr>
            </tbody></table>
            </td>
            </tr>
                <tr>
                    <td valign="top" align="center" style="padding:0 50px;margin:0;">
                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%">
                            <tbody><tr>
                                <td valign="top" align="center" style="padding: 0px; margin: 0px;" class="">
                                    <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody><tr>
                            <td valign="top" align="center" style="padding:0;margin:0;">
                                <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line" style="margin: 0px; padding: 0px;">
                                    <tbody><tr>
                                        <td valign="top" align="center" style="padding: 25px 0px; margin: 0px;"><div style="height:1px;line-height:1px;border-top-width:1px; border-top-style:solid;border-top-color:#ffffff;">
                                                <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1" height="1" style="display:block;" createnew="true">
                                            </div></td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr><tr><td align="center" valign="top" style="margin: 0px; padding: 0px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text" class="text-block"><tbody><tr><td align="left" valign="top" class="lh-1" style="padding: 10px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15;"><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:18px;font-weight:bold;">Please Click The Link Below To Continue To Your Account</span></td></tr></tbody></table></td></tr><tr>
                                                    <td valign="top" align="center" style="padding:0;margin:0;">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line" style="margin: 0px; padding: 0px;">
                                                            <tbody><tr>
                                                                <td valign="top" align="center" style="padding: 10px 0px; margin: 0px;"><div style="height:1px;line-height:1px;border-top-width:1px; border-top-style:solid;border-top-color:#ffffff;">
                                                                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1" height="1" style="display:block;">
                                                                    </div></td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                                <tr><td align="center" valign="top" style="margin: 0px; padding: 0px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text" class="text-block empty-class"><tbody><tr><td align="left" valign="top" class="lh-1" style="padding: 10px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15;"><table border="0" cellpadding="0" cellspacing="0" align="left" data-editable="image" data-mobile-width="1" style="max-width: 100% !important;" width="610" id="edi99m6j">
                                    <tbody><tr>
                                        <td valign="top" align="left" style="display: inline-block; padding: 0px 10px 10px 0px; margin: 0px;" class="tdBlock"><img src="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611688703.png?img1509488746619" alt="" width="600" border="0" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block; width: 100%; max-width: 100% !important;" data-src="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611688703.png|600|146|600|146|0|0|1" createnew="true" data-origsrc="https://app.getresponse.com/images/common/templates/messages/1174/1/img/1174_02.png"></td>
                                    </tr>
                                </tbody></table><span style="color: rgb(38, 38, 38); font-family: Arial, Helvetica, sans-serif; font-size: 18px; font-weight: bold;">link here</span></td></tr></tbody></table></td></tr><tr>
                                                    <td valign="top" align="center" style="padding:0;margin:0;">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line" style="margin:0;padding:0;">
                                                            <tbody><tr>
                                                                <td valign="top" align="center" style="padding: 5px 0px; margin: 0px; border-bottom: 2px solid rgb(247, 247, 247); border-left: 2px solid rgb(247, 247, 247); border-right: 2px solid rgb(247, 247, 247);"><div style="height:1px;line-height:1px;border-top-width:1px; border-top-style:solid;border-top-color:#ffffff;">
                                                                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1" height="1" style="display:block;">
                                                                    </div></td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" align="center" style="padding:0;margin:0;">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" data-editable="line" style="margin: 0px; padding: 0px;">
                                                            <tbody><tr>
                                                                <td valign="top" align="center" style="padding: 10px 0px; margin: 0px;"><div style="height:1px;line-height:1px;border-top-width:1px; border-top-style:solid;border-top-color:#ffffff;">
                                                                        <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" alt="" width="1" height="1" style="display:block;">
                                                                    </div></td>
                                                            </tr>
                                                        </tbody></table>
                                                    </td>
                                                </tr><tr><td align="center" valign="top" style="margin: 0px; padding: 0px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text" class="text-block"><tbody><tr><td align="left" valign="top" class="lh-1" style="padding: 10px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15;"><span style="font-family:Arial,Helvetica,sans-serif;color:#262626;font-size:18px;font-weight:bold;">Your Login Info</span><br><span style="color: rgb(38, 38, 38); font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Email: lucache95@gmail.com</span><div><span style="color: rgb(38, 38, 38); font-family: Arial, Helvetica, sans-serif; font-size: 14px;">Password: mypassword</span></div></td></tr></tbody></table></td></tr>
                                            </tbody></table>
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                    <tr><td align="center" valign="top" style="margin: 0px; padding: 0px;"><table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" data-editable="text" class="text-block empty-class"><tbody><tr><td valign="top" align="left" class="lh-1" style="padding: 99px 10px 10px; margin: 0px; font-size: 16px; font-family: Times New Roman, Times, serif; line-height: 1.15;"><table border="0" cellpadding="0" cellspacing="0" align="left" data-editable="image" style="margin: 0px; padding: 0px;mso-table-rspace: 7pt" data-mobile-width="1" width="160" id="edijj696"><tbody><tr><td valign="top" align="left" style="display: inline-block; padding: 0px 10px 10px 0px; margin: 0px;" class="tdBlock"><img src="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611690603.jpg?img1509488746619" width="150" style="border-width: 0px; border-style: none; border-color: transparent; font-size: 12px; display: block;" data-src="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611690603.jpg|480|589|480|589|0|0|3.2" data-origsrc="https://multimedia.getresponse.com/getresponse-hRl8N/photos/611690503.jpg?_ga=2.206658515.1517448460.1509487344-68529623.1509487344" height="184"></td></tr></tbody></table><span style="font-weight: bold;"><font style="font-size: 23px;" size="23"><span style="font-family: Arial, Helvetica, sans-serif; color: rgb(38, 38, 38);">Lucas Senecha</span><span style="color: rgb(38, 38, 38); font-family: Arial, Helvetica, sans-serif; background-color: transparent;">l</span></font></span><div>Client Account Manager, System Operations Manager.</div><div>(306) 571 9041</div><div>lucas@clientcaller.com</div><div>www.clientcaller.com</div><div>www.myclientcaller.com</div></td></tr></tbody></table></td></tr></tbody></table>
                </td>
            </tr>
            </tbody></table>
        ';

        sendEmail($email, $subject, $msg);

    }


?>