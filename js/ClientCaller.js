var ClientCaller = {

    init:function() {

        $('#charge_setup_fee').on('click', function() {
            ClientCaller.chargeSetupFee($('#charge_user_id').val());
        });

        $('#manually_charge_user').on('click', function() {
            ClientCaller.chargeManualFee($('#charge_user_id').val(), $('#charge_amount').val() );
        });

        $('#user_select_billing').on('change', function() {
            window.location = "admin_billing.php?current_user=" + $(this).val();
        });

        $('#user_select').on('change', function() {
            window.location = "admin_chat.php?current_user=" + $(this).val();
        });

        $('.delete-voicemail').on('click', function() {
            ClientCaller.deleteVoicemail($(this).attr('id'));
        });

        $('.edit-voicemail-name').on('click', function() {
            console.log('clicked');
            $(this).siblings('input').removeAttr('disabled');
            $(this).siblings('input').focus();
        });

        $('.voicemail-name').keyup( function(e) {
            if (e.keyCode == 13) {
                ClientCaller.editVoicemail($(this).attr('id'), $(this).val());
            }
        });

        $('#search_users_admin_accounts').keyup( function(e) {
            if (e.keyCode == 13) {
                ClientCaller.searchUsersAdminAccounts($(this).val());
            }
        });

        $('#search_users_admin_chat').keyup( function(e) {
            if (e.keyCode == 13) {
                ClientCaller.searchUsersAdminChat($(this).val());
            }
        });

        $('#search_clients').keyup( function(e) {
            if (e.keyCode == 13) {
                ClientCaller.searchClients($(this).val());
            }
        });

        $('.contacted-client').on('click', function() {
            ClientCaller.markCallAsSeen($(this).attr('id'));
        });

        $('.view-call-details').on('click', function() {
            $(this).siblings('.call-details').slideToggle();
        });

        $('#api-key-select').on('change', function() {
            var api_key = $('#api-key-select option:selected').val();
            window.location = "calling_apis.php?api_key="+api_key;
        });

        $('.conversation-box').on('click', function() {
            window.location = "admin_message.php?user_id="+$(this).attr('id');
        });

        $('#admin-fee-div button').on('click', function() {
            ClientCaller.chargeSetupFee($(this).attr('id'));
        });

        $(document).on('click', '.activate-voicemail-button', function() {
            console.log("clicked");
            //last_id, id
            ClientCaller.activateVoicemail($('.active-voicemail-button').attr('id'), $(this).attr('id'));
        });

        $('.alertbar #upload-client-list').on('click', function() {
            window.location = 'uploadCSV.php';
        });

        //$('.topbar').fadeIn('slow');
        if ($('#payment-form').length > 0) {
            ClientCaller.stripejs();
        }

        //ClientCaller.underlineNavItem();
        ClientCaller.underlineSubnavItem();

        if ($('.custom-nav .dropdown-content').length > 0) {
            $('.custom-nav .dropdown-content').each(function() {
                $(this).css({'left' : $(this).parent('.dropdown-element').position().left});
            });
        }
        var path = window.location.href;
        var page = path.substring(path.lastIndexOf('/') + 1).split('.')[0];

        if (page == 'dashboard') {
            $('.topbar').css({ 'height' : '140px', 'margin-top' : '70px' });
            //$('.message-sidebar').css({ 'top' : '210px' });
            //$('.message-sidebar #scroll-div').animate( { scrollTop: $('.message-sidebar #scroll-div')[0].scrollHeight }, 'slow' );
            //$('.message-sidebar #scroll-div').scrollTop($('.message-sidebar #scroll-div')[0].scrollHeight);
            $('.alertbar').removeClass('hidden');
        } else if (page == 'support') {
            $('.message-sidebar').css({ 'top' : '140px', 'height' : 'calc(100%-140px)' });
        }

        //Login form
        $('#loginSubmit').click(function() {
            event.preventDefault(); //Prevents page from reloading (ajax request)
            ClientCaller.login($('#login-form')); //call login function and post .login-form's details
        });

        //Register form
        $('#registerSubmit').click(function() {
            event.preventDefault(); //Prevents page from reloading (ajax request)
            // if (ClientCaller.checkPasswordsMatch()) {
                ClientCaller.register($('#register-form')); //call register function and post .register-form's details
            // }
        });

        //Agile Register form
        $('#agileRegisterSubmit').click(function() {
            event.preventDefault(); //Prevents page from reloading (ajax request)
            if (ClientCaller.checkPasswordsMatch()) {
                ClientCaller.agileRegister($('#register-form')); //call register function and post .register-form's details
            }
        });

        $('#forgot-password-submit').on('click', function() {
            event.preventDefault();
            ClientCaller.forgotPassword($('#forgot-password-form'));
        });

        $('#reset-password-submit').click(function() {
            event.preventDefault(); //Prevents page from reloading (ajax request)
            if (ClientCaller.checkPasswordsMatch()) {
                ClientCaller.resetPassword($('#reset-password-form')); //call register function and post .register-form's details
            }
        });

        $('#update-password-submit').on('click', function() {
            event.preventDefault();
            if (ClientCaller.checkPasswordsMatch()) {
                ClientCaller.updatePassword($('#update-password-form'));
            }
        });

        $('#update-profile-submit').on('click', function() {
            event.preventDefault();
            ClientCaller.updateProfile($('#update-profile-form'));
        });

        $('#update-billing-address-submit').on('click', function() {
            event.preventDefault();
            ClientCaller.updateBillingAddress($('#update-billing-address-form'));
        });

        $('#calling-info-submit').on('click', function() {
            event.preventDefault();
            ClientCaller.insertCallingInfo($('#calling-info-form'));
        });

        $('#schedule-call-submit').on('click', function() {
            event.preventDefault();
            ClientCaller.scheduleCall($('#schedule-call-form'));
        });

        $('#voicemailSubmit').on("click", function() {
            //$(this).val('Calling...');
            //event.preventDefault();
            //ClientCaller.recordVoicemail($('#voicemail-form'));
        });

        //Add client form
        $('#addClientSubmit').on("click", function() {
            event.preventDefault();
            ClientCaller.addClient($('#add-client-form'));
        });

        $('#upload-client-csv').on('click', function() {
            event.preventDefault();
            $('div.error').text("");
            ClientCaller.uploadCSV($('#file'));
        });

        $('#import-client-csv').on('click', function() {
            event.preventDefault();
            ClientCaller.importCSV();
        });

        //Edit client form
        $('#editClientSubmit').on("click", function() {
            event.preventDefault();
            ClientCaller.updateClient($('#edit-client-form'));
        });

        //Delete client (viewClient.php)
        $('#deleteClient').on("click", function() {
            var ids = [];
            ids.push($('#client_id').val());
            ClientCaller.deleteClients(ids);
        });

        $('#uploadClientList').click(function() {
            window.location.href = "uploadCSV.php";
        });

        $('#recordVoicemail').click(function() {
            window.location.href = "recordVoicemail.php";
        });

        $('#badNumbers').click(function() {
            window.location.href = "calls.php?type=bad_number";
        });

        $('#callbackRequests').click(function() {
            window.location.href = "calls.php?type=callback_request";
        });

        $('button.call_action').on("click", function() {
            ClientCaller.callSeen($(this));
        })

        $('.view-call-session').on('click', function() {
            window.location = "call_session.php?call_session_id=" + $(this).attr('id');
        });

        //select all checkboxes when select all is pressed
        $('#select_all_checkbox').on("click", function() {
            if ($(this).is(":checked")) {
                $('.checkbox').prop('checked', true);
            } else {
                $('.checkbox').prop('checked', false);
            }
        });

        //go to client page if clicked
        $('tr.client-row .view-client').on('click', function() {
            window.location.href = "viewClient.php?client_id="+$(this).parent().attr('id');
        });

        //when delete button clicked
        $('#delete_client_button').on("click", function() {

            var ids = [];

            $('.checkbox').each(function() {
                if ($(this).is(":checked")) {
                    var id = $(this).attr('id');
                    ids.push(id);
                    console.log(id);
                }
            });

            if (ids.length > 0) {

                if (window.confirm("Are you sure you want to delete these clients?\n"+ids.join("\n"))) {
                    ClientCaller.deleteClients(ids);
                }

            }

        });

        $('#do_not_contact_client_button').on('click', function() {

            var ids = [];

            $('.checkbox').each(function() {
                if ($(this).is(":checked")) {
                    var id = $(this).attr('id');
                    ids.push(id);
                    console.log(id);
                }
            });

            if (ids.length > 0) {

                if (window.confirm("Are you sure you want to mark these clients as 'Do not call'?\n"+ids.join("\n"))) {
                    ClientCaller.doNotContactClients(ids);
                }

            }

        });

        $('#delete_call_button').on('click', function() {

            var ids = [];
            
            $('.checkbox').each(function() {
                if ($(this).is(":checked")) {
                    var id = $(this).attr('id');
                    ids.push(id);
                    console.log(id);
                }
            });

            if (ids.length > 0) {

                if (window.confirm("Are you sure you want to delete these calls?\n"+ids.join("\n"))) {
                    ClientCaller.deleteCalls(ids);
                }

            }

        });

        //import dropdown menu (csv & phoneburner)
        $('#import').on('mouseenter', function() {
            $('#import_options').removeClass('hidden');
        });
        $('#import').on('mouseleave', function() {
            $('#import_options').addClass('hidden');
        });

        //import phoneburner clients
        $('#importPhoneburner').on("click", function() {
            var clients = [];
            $('.checkbox').each(function() {
                if ($(this).is(":checked")) {
                    var client_elements = $(this).parent().siblings();
                    var client = {
                        id:client_elements.eq(0).text(),
                        name:client_elements.eq(1).text(),
                        phone:client_elements.eq(2).text(),
                        feedback:client_elements.eq(3).val()
                    };

                    clients.push(client);
                }
            });
            ClientCaller.insertClients(clients);
        });

        //fake, pretty button which triggers the real, invisible file picker
        $('#choose_file').on('click', function() {
            $('#file').trigger('click');
            $('#choose_file').val("No file chosen");
        });

        //choose csv
        $('#file').on('change', function() {
            if ($(this).get(0).files != null) {
                $('#choose_file').val($(this).get(0).files[0]["name"]);
            }
        });

        //profile picture to go to profile.php
        $('#profile_pic').on('click', function() {
            window.location.href = "profile.php";
        });
        
        if ($('#calendar-div').length > 0) {
            ClientCaller.getScheduledCalls();
        }

        if ($('#dashboard-pie-chart').length > 0) {
            ClientCaller.setupPieChart();
        }

    },

    agileRegister:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        //POST 'serializedData' to 'parseRegister.php'
        var request = $.post("Agile.php", serializedData, function(response) {
            //console.log(response);
        });

        

        //Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                ClientCaller.register($('#register-form'));
            } else {
                var message = obj.message || "Registration failed";
                $('div.error').html(message);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: "+
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    register:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        //POST 'serializedData' to 'parseRegister.php'
        var request = $.post("parseRegister.php", serializedData, function(response) {
            //console.log(response);
        });

        

        //Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "billing_info.php";
                var to = obj.user.user_id;

                var content = "Hello " + obj.user.first_name + " - If you have any questions, feel free to send a message or check out our FAQ!";
                ClientCaller.sendAdminMessage(to, content);

            } else {
                var message = obj.message || "Registration failed";
                $('div.error').html(message);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown) {
            // Log the error to the console
            console.error(
                "The following error occurred: "+
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    login:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        //POST 'serializedData' to 'parseLogin.php'
        var request = $.post("parseLogin.php", serializedData, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "dashboard.php";
            } else {
                var message = obj.message || "Login failed";
                $('div.error').html(message);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // Log the error to the console
            console.error(
                "The following error occurred: "+
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            inputs.prop("disabled", false);
        });

    },

    forgotPassword:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        //POST 'serializedData' to 'parseLogin.php'
        var request = $.post("parseForgotPassword.php", serializedData, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "login.php";
            } else {
                var message = obj.message || "Update password failed";
                $('div.error').html(message);
            }
        });

        // Callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // Log the error to the console
            console.error(
                "The following error occurred: "+
                textStatus, errorThrown
            );
        });

        // Callback handler that will be called regardless
        // if the request failed or succeeded
        request.always(function () {
            inputs.prop("disabled", false);
        });

    },

    resetPassword:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        var request = $.post("parseResetPassword.php", serializedData, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('div.error').css({ "color" : "green" }).html("Password updated");
            } else {
                var message = obj.message || "Update password failed";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    updatePassword:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        var request = $.post("parseUpdatePassword.php", serializedData, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('div.error').css({ "color" : "green" }).html("Password Updated");
            } else {
                var message = obj.message || "Update password failed";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    updateProfile:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        var request = $.post("updateProfile.php", serializedData, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('div.error').css({ "color" : "green" }).html("Profile updated");
            } else {
                var message = obj.message || "Profile update failed";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    checkPasswordsMatch:function(num = 0) {

        var pw1 = $('#pw1').val();
        var pw2 = $('#pw2').val();
        var error = $('div.error');

        if (num == 1 && pw2.length == 0) {
            return false;
        }

        if (pw1 != pw2) {
            error.html("Passwords do not match");
            return false;
        } else if (pw1.length < 6 || pw2.length < 6) {
            error.html("Password needs to be at least 6 characters");
        } else {
            error.html("");
            return true;
        }

    },

    //helper function for sending admin messages
    sendAdminMessage:function(to, content) {

        var from = 'admin';

        ClientCaller.sendMessage(from, to, content);

    },

    sendMessage:function(from, to, content) {

        var request = $.post("sendMessage.php", {from_user_id: from, to_user_id: to, content: content}, function(response) {
            console.log(response);
        });

    },

    insertClients:function(clients) {

        var request = $.post("insertClients.php", {clients: clients}, function(response) {
            console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "clients.php";
            } else {
                var message = obj.message || "Import failed";
                $('div.error').html(message);
            }
        });

    },

    addClient:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        var request = $.post("parseAddClient.php", serializedData, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "clients.php";
            } else {
                var message = obj.message || "Import failed";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    searchClients:function(query_string) {
        window.location = "clients.php?query_string="+query_string;

        /*var request = $.post("searchClients.php", {query_string:query}, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "clients.php?query_string="+query_string;
            } else {
                var message = obj.message || "Import failed";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });*/

    },

    searchUsersAdminChat:function(query_string) {

        window.location = "admin_chat.php?query_string=" + query_string;

    },

    searchUsersAdminAccounts:function(query_string) {

        window.location = "admin_accounts.php?query_string=" + query_string;

    },

    updateClient:function(form) {
        
        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        var request = $.post("parseUpdateClient.php", serializedData, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "clients.php";
            } else {
                var message = obj.message || "Update failed";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    deleteClients:function(ids) {

        var request = $.post("deleteClients.php", {client_ids: ids}, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "clients.php";
            } else {
                var message = obj.message || "Delete failed";
                $('div.error').html(message);
            }
        });

    },

    doNotContactClients:function(ids) {
        
        var request = $.post("doNotContactClients.php", {client_ids: ids}, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "clients.php";
            } else {
                var message = obj.message || "Request failed";
                $('div.error').html(message);
            }
        });

    },

    uploadCSV:function(file) {

        var fd = new FormData();
        fd.append('file', file.get(0).files[0]);

        var request = $.ajax({
            url: "parseCSV.php",
            method: 'POST',
            data: fd,
            processData: false,
            contentType: false
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                var table_head = "<tr>";
                var line_count = 0;

                obj.lines.forEach(function(item) {
                    if (Object.keys(item).length > line_count) {
                        line_count = Object.keys(item).length;
                    }
                });

                var i = 0;
                while (i < line_count) {
                    table_head += "<th>" +
                    "<select name='col" + i + "'>" +
                    "<option value='empty'>Empty</option>" +
                    "<option value='first_name'>First Name</option>" +
                    "<option value='last_name'>Last Name</option>" +
                    "<option value='phone_number'>Phone Number</option>" +
                    "<option value='email'>Email</option>" +
                    "</select>" +
                    "</th>";
                    i++;
                }
                table_head += "</tr>";
                $('#result-table thead').html(table_head);
                var table_body = "";
                obj.lines.forEach(function(line) {
                    var table_row = "<tr>";
                    line.forEach(function(field) {
                        table_row += "<td>";
                        if (field != null) {
                            var field = field.trim();
                        }
                        table_row += field;
                        table_row += "</td>";
                    });
                    table_row += "</tr>";
                    table_body += table_row;
                });
                $('#result-table tbody').html(table_body);
                $('html, body').animate({ scrollTop: $('#csv-scroll').offset().top - 70 }, 'slow');
                $('#csv-results').removeClass('hidden');
            } else {
                var message = obj.message || "Import failed";
                $('div.error').html(message);
            }
        });

    },

    importCSV:function() {

        var header_row = $('#result-table').find('thead').find('tr');

        //indexes for first name, last name, email, phone
        var fn_index; 
        var ln_index;
        var e_index;
        var pn_index;

        header_row.find('th').each(function(index) {

            var column_name = $(this).find('select').val();

            switch (column_name) {
                case "first_name":
                    fn_index = index;
                    break;
                case "last_name":
                    ln_index = index;
                    break;
                case "email":
                    e_index = index;
                    break;
                case "phone_number":
                    pn_index = index;
                    break;
                default:
                    break;
            }

        });

        var clients = [];

        var body_rows = $('#result-table').find('tbody').find('tr');

        var clients = [];

        body_rows.each(function(index) {

            var client = {};
            
            $(this).find('td').each(function(i) {
                switch (i) {
                    case fn_index:
                        client.first_name = $(this).text();
                        break;
                    case ln_index:
                        client.last_name = $(this).text();
                        break;
                    case e_index:
                        client.email = $(this).text();
                        break;
                    case pn_index:
                        client.phone_number = $(this).text();
                        break;
                    default:
                        break;
                }
            });

            clients.push(client);

        });

        var request = $.post("insertClients.php", {clients: clients}, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "clients.php";
            } else {
                var message = obj.message || "Error";
                $('div.error').html(message);
            }
        });

    },

    callSeen:function(el) {

        var id = el.attr('id');

        var request = $.post("parseCallSeen.php", {call_id: id}, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                el.parent().html("<span>✓</span>"); //replace with nicer checkmark
            } else {
                var message = obj.message || "Error";
                $('div.error').html(message);
            }
        });

    },

    deleteCalls:function(ids) {
        
        var request = $.post("deleteCalls.php", {call_ids: ids}, function(response) {
            //console.log(response);
        });

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                window.location = "calls.php";
            } else {
                var message = obj.message || "Delete failed";
                $('div.error').html(message);
            }
        });

    },

    underlineSubnavItem:function() {
        
        var results = new RegExp('[\?&]' + "type" + '=([^&#]*)').exec(window.location.href);
        if (results==null) {
            $('#callback_requests').addClass('underline');
            $('#callback_requests a').removeClass('light-gray');
            $('#callback_requests a').addClass('dark-gray');
        } else {
            var id = decodeURI(results[1]) || 0;
            var element = $("#"+id);
            element.addClass('underline');
            element.find('a').removeClass('light-gray');
            element.find('a').addClass('dark-gray');
            //$('#all').removeClass('underline');
        }

    },

    underlineNavItem:function() {

        //underline the navigation item for the current page
        var path = window.location.href;
        var page = path.substring(path.lastIndexOf('/') + 1);

        //holy shit this code coming up is sick
        var aElement = $("a[href*='"+page+"']"); //split url after ? to check if type=callback_requests or bad_numbers
      /*if (!aElement.length) { //if element is null after ^, then check for url
            aElement = $("a[href*='"+page.toLowerCase().replace(' ', '_')+"']");
        }*/
        aElement.addClass('underline');
        //$('html, body').scrollTop(0);
        //aElement.prop("disabled", true);

    },

    deleteVoicemail:function(id) {

        var request = $.post('deleteVoicemail.php', {voicemail_id:id}, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                //update buttons
                $('#'+id).parents('.voicemail-box').hide();
                
            } else {
                var message = obj.message || "Error";
                $('div.error').html(message);
            }
        });

    },

    editVoicemail:function(id, newName) {

        var request = $.post('editVoicemailName.php', {voicemail_id:id, name:newName}, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('span.error').css({'color':'green'}).html("Saved");
                $('span.error').fadeIn('fast');
                setTimeout(function() {
                    $('span.error').fadeOut(1000);
                }, 2000);
            } else {
                var message = obj.message || "Error";
                $('div.error').html(message);
            }
        });

    },

    activateVoicemail:function(last_id, id) {

        var request = $.post("activateVoicemail.php", {last_voicemail_id: last_id, voicemail_id: id}, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                //update buttons
                var last = $('#'+last_id);
                var current = $('#'+id);
                last.removeClass('active-voicemail-button');
                last.addClass('activate-voicemail-button');
                last.text("Select");
                current.removeClass('activate-voicemail-button');
                current.addClass('active-voicemail-button');
                current.text("Active");
            } else {
                var message = obj.message || "Error";
                $('div.error').html(message);
            }
        });

    },

    insertCallingInfo:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        //POST 'serializedData' to 'parseRegister.php'
        var request = $.post("insertCallingInfo.php", serializedData, function(response) {
            //console.log(response);
        });

        //Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('div.error').css({ 'color': 'green' }).html("Saved");
                setTimeout(function() {
                    $('div.error').html("Redirecting to dashboard...");
                }, 2000);
                setTimeout(function() {
                    window.location = 'dashboard.php';
                }, 4000);
            } else {
                var message = obj.message || "Failed to update";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },
    
    updateBillingAddress:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        //POST 'serializedData' to 'parseRegister.php'
        var request = $.post("update_billing_address.php", serializedData, function(response) {
            //console.log(response);
        });

        //Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('div.error').css({ 'color': 'green' }).html("Saved");
            } else {
                var message = obj.message || "Failed to update";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    markCallAsSeen:function(call_id) {

        var request = $.post("markCallAsSeen.php", {call_id:call_id}, function(response) {
            //console.log(response);
        });

        //Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('#'+call_id).prop('disabled', true);
                $('#'+call_id).parent().parent().removeClass('bg-light-blue');
                $('#'+call_id).text('Contacted');
            } else {
                
            }
        });

    },

    getCallbackRequests:function(number = 0) {



    },

    getBadNumbers:function(number = 0) {

    },

    scheduleCall:function(form) {

        var serializedData = form.serialize();
        var inputs = form.find("input, select, button, textarea");
        inputs.prop("disabled", true); //disable buttons etc so can't double submit

        //POST 'serializedData' to 'parseRegister.php'
        var request = $.post("scheduleCall.php", serializedData, function(response) {
            //console.log(response);
        });

        //Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                $('div.error').css({ 'color': 'green' }).html("Call successfully scheduled");
                ClientCaller.getScheduledCalls(true);
            } else {
                var message = obj.message || "Failed to schedule";
                $('div.error').html(message);
            }
        });

        request.always(function () {
            //console.log("finished register request");
            inputs.prop("disabled", false);
        });

    },

    getScheduledCalls:function(refresh = false) {

        var request = $.get("getScheduledCalls.php", function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                var events = [];
                obj.scheduled_calls.forEach(function(event) {
                    events.push(event);
                });
                if (refresh) { $('#calendar-div').fullCalendar('addEventSource', [events[events.length-1]]); }
                ClientCaller.setupCalendar(events);
            } else {

            }
        });

    },

    setupCalendar:function(events) {

        $('#calendar-div').fullCalendar({
            events: events,
            defaultView: "listMonth"
        });

    },

    chargeSetupFee:function(user_id) {

        var request = $.post("charge_user.php", {charge_user_id: user_id}, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                console.log("success");
                $('#admin-fee-div #'+user_id).html("Successfully charged");
            } else {
                //window.location = "billing_info.php?user="+user_id;
                $('#admin-fee-div #'+user_id).html("Charge failed: "+obj.message);
            }
        });

    },

    chargeManualFee:function(user_id, charge_amount) {

        var request = $.post("charge_user.php", {
            charge_user_id:user_id,
            posted_charge_amount:charge_amount
        }, function(response) {
            //console.log(response);
        });

        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                console.log("success");
                $('#admin-fee-div #'+user_id).html("Successfully charged");
            } else {
                //window.location = "billing_info.php?user="+user_id;
            }
        });

    },

    stripejs:function() {

        //live pk_live_VCgE3GSswZvLVxNjvWLxTukA
        //test pk_test_MbbVkKXO0c4eJ5TwcVoaKSQ1

        // Create a Stripe client
        var stripe = Stripe('pk_live_VCgE3GSswZvLVxNjvWLxTukA');

        // Create an instance of Elements
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
        base: {
            color: '#32325d',
            lineHeight: '24px',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
            color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
        };

        // Create an instance of the card Element
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server
                    stripeTokenHandler(result.token);
                }
            });
        });

        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);
          
            // Submit the form
            form.submit();
        }

    },

    /*setupPieChart:function() {
        
        var request = $.get("getCallSessions.php", function(response) {
            //console.log(response);
        });

        var total_calls = 0;
        var bad_numbers = 0;
        var voicemails = 0;
        var feedback = 0;
        var callback_requests = 0;
        var do_not_calls = 0;
        var not_interested = 0;
        var busy_phones = 0;
        var no_results = 0;

        // Callback handler that will be called on success
        request.done(function(response, textStatus, jqXHR) {
            var obj = $.parseJSON(response);
            if (obj.success) {
                obj.call_sessions.forEach(function(cs) {
                    total_calls += parseInt(cs.total_calls);
                    bad_numbers += parseInt(cs.bad_number);
                    voicemails += parseInt(cs.voicemail);
                    feedback += parseInt(cs.feedback);
                    callback_requests += parseInt(cs.callback_request);
                    do_not_calls += parseInt(cs.do_not_call);
                    not_interested += parseInt(cs.not_interested);
                    busy_phones += parseInt(cs.busy_phone);
                    no_results += parseInt(cs.no_result);
                });
                google.charts.load('current', {'packages':['corechart']});
                google.charts.setOnLoadCallback(drawChart);
            } else {
                var message = obj.message || "Error loading pie chart";
                $('div.error').html(message);
            }
        });
  
        function drawChart() {
  
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Number'],
                ['Bad Numbers', bad_numbers],
                ['Voicemails', voicemails],
                ['Callback Requests',  callback_requests],
                ['Do Not Call', do_not_calls],
                ['Not Interested', not_interested],
                ['Busy Phone', busy_phones],
                ['No Result', no_results]
            ]);
    
            var options = {
                title: 'Total calls: '+total_calls,
                backgroundColor: 'transparent'
            };
    
            var chart = new google.visualization.PieChart(document.getElementById('dashboard-pie-chart'));
    
            chart.draw(data, options);

        }

    },*/

};

$(document).ready( ClientCaller.init );