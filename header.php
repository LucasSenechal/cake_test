<?php

    session_start();
    
    require_once('functions.php');

    date_default_timezone_set("America/Vancouver");

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html lang='en'>

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="title" content="Enter My Giveaway" />
    <meta name="Description" content="Enter My Giveaway" />

    <title>Enter My Giveaway</title>
<!-- I removed: <?php echo $_SERVER['PHP_SELF']; ?> -->

    <!--Styles-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel='stylesheet' type='text/css' href='css/main.css'/>
    <!--/Styles-->

    <!--Scripts-->
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script src='js/ClientCaller.js'></script>
    <!--/Scripts-->

    <!--Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:500" rel="stylesheet">


    <!--Full Calendar-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://momentjs.com/downloads/moment.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.js'></script>
    <link rel='stylesheet' href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" />

    <script src="https://js.stripe.com/v3/"></script>

    
    

</head>

<body>
    <div class='col-sm-1'></div>
    <!-- Dont show spacer on faq page becuase of webframe -->
    <?php if(strpos($_SERVER['PHP_SELF'], "faq.php") === false){ ?>
    <div class='container col-sm-10'>
    <div class="spacer-50"></div>
    <?php } ?>