<?php 

require_once('header.php');

session_start();

//require our database connection file
require('conn.php');


$first_name = $conn->real_escape_string($_POST['first_name']);
$last_name = $conn->real_escape_string($_POST['last_name']);
$email = strtolower($conn->real_escape_string($_POST['email']));



//query database to check if email has already been registered
$query = "SELECT * FROM `Contest_Entry` WHERE email='$email'";

//declare boolean which will determine if user was successfully registered
$success = false;
$message = null;

//check if query executes
if ($result = $conn->query($query)) {

    //check if query returned any results
    //if the query returned no results, then the email is available to register
    if ($result->num_rows < 1) {

        $date_created = date("Y/m/d");

        //setup db insert query
        $insertQuery = 
        "INSERT INTO `Contest_Entry`( `first_name`, `last_name`, `email` ) 
        VALUES ( '$first_name', '$last_name', '$email' )";

        //execute query
        if ($insertResult = $conn->query($insertQuery)) {
            
            $_SESSION['email'] = $email;
            $_SESSION['first_name'] = $first_name;
            $_SESSION['last_name'] = $last_name;
            
            $success = true;
            $message = "";

            //sendWelcomeEmail($email);

        } else {
            $message = $insertQuery;
            //$message = "Could not insert into database";
        }

    } else {
        $message = "This email is already associated with an account";
    }

} else {
    $message = "Could not execute query";
}

// echo json_encode(array('user' => array('user_id' => $_SESSION['user_id'], 'first_name' => $_SESSION['first_name']), 'success' => $success, 'message' => $message));

$conn->close();



?>

<div>



    <form class='form-box' action="Page_3.php" method="POST" id="payment-form">

        <div class="form-row">

                <div>
                    <h2 class='thanks-text' >You've been Entered!</h2>
                </div>  

                <div>
                    <h1 class='header-text' >Double Your Chances of Winning!</h1>
                </div>  

                <div>
                    <h2 class='tagline-text' >Enter your phone number to receive another entry!</h2>
                </div>  

                <div class='form-box-size'>
                    <input class='form-control' type='text' placeholder="Phone Number" name='phone_number' required />
                </div>

                <div class='spacer-50'></div>

                <input type='submit' value='Add a Second Entry' class='submit' />

    </form>

  </div>      